<?php

/**
 * This script executes the moodle cron for UW Moodle
 *
 * For UW moodle we need to check to see if we need to purge the caches before running cron.  This is only a
 * useful check if the moodle cron machine has a different moodle cache than the rest of moodle, which
 * is, by the way, an officially unsupported configuration.
 * Hopefully with moodle 2.6 we'll have cluster-aware caches and we won't need to do this.
 *
 * @author     Matt Petro
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/admin/uwmoodle_admin_helper.php');

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false, 'nowait'=>false),
                                               array('h'=>'help', 'n'=>'nowait'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help']) {
    $help =
"Execute UW Moodle cron.

Options:
-h, --help            Print out this help
-n, --nowait          Run the cron immediately, skipping the splay sleep.

Example:
\$sudo -u www-data /usr/bin/php cron.php
";

    echo $help;
    die;
}

// Run the cron
uwmoodle_admin_helper::cron_run($options['nowait']);
