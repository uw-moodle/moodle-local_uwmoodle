<?php

/**
 * Admin helper class.
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/cronlib.php');
require_once($CFG->dirroot.'/local/uwmoodle/util/uwmoodle_util_helper.php');

abstract class uwmoodle_admin_helper {

    /**
     * Check if these is too much concurrency of cron executions.  Throws an exception if
     * there is an error, or there are too many cron jobs running.
     */
    protected static function pre_cron_check_concurrency() {
        global $DB, $CFG;

        if (class_exists('\core\task\task_base')) {
            // The task API exists, so we can support multiple simultaneous runs.
            $maxconcurrency = 5;
        } else {
            // Old style cron.  Don't allow concurrency.
            $maxconcurrency = 1;
        }
        mtrace("Max concurrency = $maxconcurrency");

        // Try to aquire one of the 1..$maxconcurrency db named locks.
        // There is of course a more scalable way to do this, but this way has the advantage of
        // self-healing if something goes wrong.  Mysql will always release a named lock if the connection dies.

        // TODO: check if this conflicts with the session lock.  Does Cron make a session lock?

        for ($i = 0 ; $i < $maxconcurrency ; ++$i) {
            $fullname = $CFG->dbname.'-'.$CFG->prefix.'-cron-'.$i;
            // Try to get lock $i with no waiting.
            $result = $DB->get_record_sql("SELECT 1 as id, GET_LOCK(?, 0) as value", array($fullname));
            if (!$result) {
                throw new Exception("ERROR in aquiring cron lock.");
            }
            if ($result->value == 1) {
                // Got lock.  It'll get released automatically when this process exits.
                mtrace("Jobs running: $i");
                mtrace("Aquired lock");
                return;
            }
        }
        // If we get here, all the locks were busy.
        mtrace("ERROR: Unable to aquire a cron lock.   Max concurrency is $maxconcurrency.");
        exit(1);
    }

    /**
     * Sleep some site-dependent amount of time.  This is to prevent all of the moodle crons from
     * running at the same time.
     */
    protected static function pre_cron_sleep() {
        global $CFG;

        /**
         * Pick a delay, while trying to keep the major sites separate.
         *
         * Ay sites map to 0-19 seconds
         * Innov sites map to 25-34 seconds
         * Special is 40 seconds
         * Other sites map to 45-54 seconds.
         */

        $hostname = parse_url($CFG->wwwroot, PHP_URL_HOST);
        $matches = array();
        if (preg_match('/^ay([0-9]+)-[0-9]+\./', $hostname, $matches)) {
            $year = $matches[1];
            $sleep = ($year*7)%20;
        } else if (preg_match('/^innov([0-9]+)-[0-9]+\./', $hostname, $matches)) {
            $year = $matches[1];
            $sleep = ($year*3)%10 + 25;
        } else if (preg_match('/^special\./', $hostname)) {
            $sleep = 40;
        } else {
            $sleep =  ( hexdec( substr(md5($hostname), -7, 7) ) % 10 ) + 45;
        }
        mtrace("Sleeping $sleep seconds...", "");
        sleep ($sleep);
        mtrace("done");

    }

    /**
     * Clear the moodle caches if necessary and then run cron.
     *
     * @param bool $nowait true to skip the splay sleep
     */
    public static function cron_run($nowait) {
        global $DB;

        // UWMOODLE-1407 - Increase recursion depth for pdf annotation plugin.
        ini_set('xdebug.max_nesting_level', 10000);

        if (!$nowait) {
            self::pre_cron_sleep();
        }
        self::pre_cron_check_concurrency();

        // UMCT-37 We're using a reduced lock expiry time, so set a script alarm.
        if (defined('DB_LOCK_FACTORY_MAXLIFETIME')) {
            mtrace("Setting alarm for ".DB_LOCK_FACTORY_MAXLIFETIME." seconds");
            pcntl_alarm(DB_LOCK_FACTORY_MAXLIFETIME);
        }

        $lastpurge = get_config('local_uwmoodle', 'lastcachepurge');
        $lastcron = get_config('local_uwmoodle', 'lastcron');
        if (!$lastcron) {
            $lastcron = 0;
        }

        mtrace('Starting UW Moodle cron');
        mtrace("Last cron execution: ".date('r', $lastcron));
        $now = time();

        if ($lastpurge && ($lastpurge >= $lastcron)) {
            mtrace('UW Moodle cluster: cache was purged');
            mtrace('Purging moodle caches');
            uwmoodle_util_helper::purge_local_caches();
        }


        set_config('lastcron', $now, 'local_uwmoodle');

        cron_run();
    }
}
