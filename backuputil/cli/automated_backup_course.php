<?php

/**
 * CLI backup of one course.
 *
 * This is normally called from the automated_backups.php script, although it can be called directly.
 * This script doesn't do the normal logic to skip backups that don't need to be done (past terms, etc.)
 *
 * Returns:
 *    0 : Backup status OK
 *    1 : Backup status ERROR, or moodle exception
 *    2 : Backup status WARNING
 *
 * @author     Matt Petro
 */

// The following define affects backup behavior so that we do an rsync, eliminates the archive zip, and eliminates DB writes so
// that we can backup from a read-only slave db
define('UWMOODLE_AUTOMATED_BACKUPS', true);

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
                                                  'course'=>false),
                                            array('h'=>'help',
                                                  'c'=>'course'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['course']) {
    $help =
"Execute backups.

This script executes course backups using the automated backup settings.  A moodle course id is required.

Options:
-h, --help            Print out this help
-c, --course          Moodle course id to backup

Example:
\$sudo -u www-data /usr/bin/php local/uwmoodle/backuputil/cli/automated_backup_course.php --course=12
";

    echo $help;
    die;
}

if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

$courseid = $options['course'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();


// Run automated backups if required.
require_once($CFG->dirroot.'/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot.'/local/uwmoodle/backuputil/uwmoodle_backup_helper.php');
$backupstatus = uwmoodle_backup_helper::run_automated_course_backup($courseid);

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");

if ($backupstatus == backup_cron_automated_helper::BACKUP_STATUS_OK) {
    exit(0); // ok
} else if ($backupstatus == backup_cron_automated_helper::BACKUP_STATUS_WARNING) {
    exit(2); // warning
}

exit(1);  // error