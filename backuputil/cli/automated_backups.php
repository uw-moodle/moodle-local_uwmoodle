<?php

/**
 * This script executes automated backups for UW Moodle
 *
 * This script is similar to the built-in moodle backup script
 * except that it parallelizes the backup by forking multiple backups at once
 *
 * @author     Matt Petro
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->libdir.'/cronlib.php');

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false, 'parallel'=>1),
                                               array('h'=>'help', 'p'=>'parallel'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || $options['parallel'] <= 0) {
    $help =
"Execute UW moodle automated backups.

This script executes automated backups completely and is designed to be
called via cron.

Options:
-p, --parallel        Number of simultaneous jobs to run (default = 1)
-h, --help            Print out this help

Example:
\$sudo -u www-data /usr/bin/php admin/cli/automated_backups.php
";

    echo $help;
    die;
}
if (CLI_MAINTENANCE) {
    echo "CLI maintenance mode active, backup execution suspended.\n";
    exit(1);
}

if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/gradelib.php');

if (!empty($CFG->showcronsql)) {
    $DB->set_debug(true);
}
if (!empty($CFG->showcrondebugging)) {
    $CFG->debug = DEBUG_DEVELOPER;
    $CFG->debugdisplay = true;
}

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n\n");

// Run automated backups if required.
require_once($CFG->dirroot.'/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot.'/local/uwmoodle/backuputil/uwmoodle_backup_helper.php');
uwmoodle_backup_helper::run_automated_backup(backup_cron_automated_helper::RUN_IMMEDIATELY, $options['parallel']);

mtrace("Automated cron backups completed correctly");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");
