<?php

/**
 * CLI backup
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions

// now get cli options
list($options, $courseids) = cli_get_params(array('help'=>false,
                                                  'destination'=>false),
                                            array('h'=>'help',
                                                  'd'=>'destination'));

$unrecognized = array();
foreach ($courseids as $courseid) {
    if (!is_number($courseid) || $courseid <= 0) {
        $unrecognized[] = $courseid;
    }
}

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['destination']) {
    $help =
"Execute backups.

This script executes course backups using the default backup settings.  A list of moodle course id's is required.

Options:
-h, --help            Print out this help
-d, --destination     Directory for generated backups

Example:
\$sudo -u www-data /usr/bin/php local/uwmoodle/backuputil/cli/backup.php --destination=/tmp/backups 12 13 15
";

    echo $help;
    die;
}

$dir = $options['destination'];

if (!file_exists($dir) || !is_dir($dir) || !is_writable($dir)) {
    echo "Unable to access destination directory.\n";
    exit(1);
}

if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/gradelib.php');

// UWMOODLE-687
define('ENABLE_ELLUMINATE_BACKUP', 1); // enable Elluminate Backup/Restore

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n\n");

// Run automated backups if required.
require_once($CFG->dirroot.'/backup/util/includes/backup_includes.php');
require_once($CFG->dirroot.'/local/uwmoodle/backuputil/uwmoodle_backup_helper.php');
uwmoodle_backup_helper::run_backup($courseids, $dir);

mtrace("Backups completed");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");