<?php

/**
 * CLI conversion of a moodle1 backup directory to moodle2 format.
 *
 * @author     Matt Petro
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
                                                  'output'=>false,
                                                  'backup'=>false),
                                            array('h'=>'help',
                                                  'o'=>'output',
                                                  'b'=>'backup'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['backup'] | !$options['output']) {
    $help =
"Convert backup directory to moodle2 format.

Options:
-h, --help            Print out this help
-b, --backup          Unzipped backup directory
-o, --output          Directory which will contain the generated backup dir

Example:
\$sudo -u www-data /usr/bin/php local/uwmoodle/backuputil/cli/convert.php --backup=/tmp/moodle1-backup/
";

    echo $help;
    die;
}

$backupdir = $options['backup'];
$outputdir = $options['output'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();

// Run automated backups if required.
require_once($CFG->dirroot.'/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot.'/local/uwmoodle/backuputil/uwmoodle_restore_helper.php');

mtrace("Converting $backupdir");
uwmoodle_restore_helper::convert($backupdir, $outputdir);

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");