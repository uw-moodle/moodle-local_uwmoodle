<?php

/**
 * Conversion of an unzipped moodle 2 course backup into a directory of user
 * profile pictures suitable for uploading to a new site via the user picture
 * upload admin util.
 *
 * @author     Matt Petro
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/backuputil/uwmoodle_restore_helper.php');

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
                                                  'output'=>false,
                                                  'backup'=>false),
                                            array('h'=>'help',
                                                  'o'=>'output',
                                                  'b'=>'backup'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['backup'] | !$options['output']) {
    $help =
"Convert backup directory into user profile images suitable for upload.

Options:
-h, --help            Print out this help
-b, --backup          Unzipped backup directory
-o, --output          Directory which will contain the generated images

Example:
\$sudo -u www-data /usr/bin/php --backup=local/uwmoodle/backuputil/cli/generateuserpictures.php --output=/tmp/user-pictures/
";

    echo $help;
    die;
}

$backupdir = $options['backup'];
$outputdir = $options['output'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();


mtrace("Converting $backupdir");
uwmoodle_restore_helper::convert_user_pictures($backupdir, $outputdir);

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");