<?php

/**
 * CLI restore
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
                                                  'backup'=>false,
                                                  'anonymize' => false,
                                                  'category' => false,
                                                  'manualenrol' => false,
                                                  'users' => false
                                                  ),
                                            array('h'=>'help',
                                                  'b'=>'backup',
                                                  'a'=>'anonymize',
                                                  'c'=>'category',
                                                  'm'=>'manualenrol',
                                                  'u'=>'users',
                                                  ));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['backup']) {
    $help =
"Execute restore.

This script executes a full course restore.

Options:
-h, --help            Print out this help
-b, --backup          Moodle backup file or directory
-a, --anonymize       Anonymize user data (optional)
-c, --category        Category ID to restore to (optional)
-m, --manualenrol     Convert all enrollments to manual (This is forced with --anonymous)
-u, --users            Include user data

Example:
\$sudo -u www-data /usr/bin/php local/uwmoodle/backuputil/cli/backup.php --backup=backup.mbz
";

    echo $help;
    die;
}


if (!file_exists($options['backup'])) {
    echo "Unable to access backup file.\n";
    exit(1);
}

if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/gradelib.php');

// UWMOODLE-687
define('ENABLE_ELLUMINATE_BACKUP', 1); // enable Elluminate Backup/Restore

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n\n");

// Run automated backups if required.
require_once($CFG->dirroot.'/backup/util/includes/restore_includes.php');
require_once($CFG->dirroot.'/local/uwmoodle/backuputil/uwmoodle_restore_helper.php');

// Anon enrollments are always manual
if ($options['anonymize']) {
    $options['manualenrol'] = true;
}

uwmoodle_restore_helper::run_restore($options);

mtrace("Restore completed");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");
