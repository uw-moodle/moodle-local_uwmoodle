<?php

/**
 * Utility helper for backups run through the cli.
 *
 * @author     Matt Petro
 */

require_once($CFG->dirroot.'/backup/util/helper/backup_cron_helper.class.php');
require_once($CFG->dirroot.'/backup/controller/backup_controller.class.php');

/**
 * This class is an abstract class with methods that can be called to aid the
 * running of backups from the cli.
 *
 * This class augments backup_cron_automated_helper.
 */
abstract class uwmoodle_backup_helper {

    /**
     * Runs the backups
     *
     * @global moodle_database $DB
     */
    public static function run_backup($courseids, $destination) {
        global $DB;

        mtrace('Running backups...');

        // This could take a while!
        @set_time_limit(0);
        raise_memory_limit(MEMORY_EXTRA);

        $courses = $DB->get_records_list('course', 'id', $courseids);
        foreach ($courses as $course) {
            mtrace('Backing up '.$course->shortname, '...');
            uwmoodle_backup_helper::launch_backup($course, $destination);
            mtrace("complete");
        }
        mtrace('Backups complete.');
    }



    /**
     * Launches backup for the given course
     *
     * @param stdClass $course
     * @return bool
     */
    public static function launch_backup($course, $destination) {
        global $USER;

        $outcome = true;
        $config = get_config('backup');
        $bc = new backup_controller(backup::TYPE_1COURSE, $course->id, backup::FORMAT_MOODLE, backup::INTERACTIVE_NO, backup::MODE_GENERAL, $USER->id);

        try {

            // backup settings
            $settings = array(
                'imscc11' => 0,
                'users' => 1,
                'anonymize' => 0,
                'role_assignments' => 1,
                'activities' => 1,
                'blocks' => 1,
                'filters' => 1,
                'comments' => 1,
                'calendarevents' => 1,
                'userscompletion' => 1,
                'logs' => 1,
                'histories' => 1,   // < 2.6
                'grade_histories' => 1, // >= 2.6
                'badges' => 1,
                'questionbank' => 1,
            );
            foreach ($settings as $name => $value) {
                if ($bc->get_plan()->setting_exists($name)) {
                    $setting = $bc->get_plan()->get_setting($name);
                    $setting->set_value($value);
                } else {
                    echo "Warning, no setting: $name\n";
                }
            }

            // Set the default filename
            $format = $bc->get_format();
            $type = $bc->get_type();
            $id = $bc->get_id();
            $users = $bc->get_plan()->get_setting('users')->get_value();
            $anonymised = $bc->get_plan()->get_setting('anonymize')->get_value();
            $bc->get_plan()->get_setting('filename')->set_value(backup_plan_dbops::get_default_backup_filename($format, $type, $id, $users, $anonymised));

            $bc->set_status(backup::STATUS_AWAITING);

            $bc->execute_plan();
            $results = $bc->get_results();
            $file = $results['backup_destination']; // may be empty if file already moved to target location
            $dir = $destination;
            $storage = 1; // put archive in specified directory only
            if (!file_exists($dir) || !is_dir($dir) || !is_writable($dir)) {
                $dir = null;
            }

            if ($file && !empty($dir) && $storage !== 0) {
                $filename = backup_plan_dbops::get_default_backup_filename($format, $type, $course->id, $users, $anonymised, !$config->backup_shortname);
                $outcome = $file->copy_content_to($dir.'/'.$filename);
                if ($outcome && $storage === 1) {
                    $file->delete();
                }
            }

        } catch (moodle_exception $e) {
            $bc->log('backup_auto_failed_on_course', backup::LOG_ERROR, $course->shortname); // Log error header.
            $bc->log('Exception: ' . $e->errorcode, backup::LOG_ERROR, $e->a, 1); // Log original exception problem.
            $bc->log('Debug: ' . $e->debuginfo, backup::LOG_DEBUG, null, 1); // Log original debug information.
            $outcome = false;
        }

        $bc->destroy();
        unset($bc);

        return $outcome;
    }

    /**
     * Run automated backups in parallel
     *
     * @param array $courses the list of moodle courseid's
     * @param int $parallel  the number of backups to run at one time
     */
    public static function run_backup_jobs(array $courses, $parallel = 1 )
    {
        global $CFG, $DB;
        mtrace("Starting backup jobs, parallization = $parallel");

        // Number of seconds to sleep while waiting for child termination.  Must be < DB idle timeout.
        $SLEEPTIME = 300;

        $forks = array();
        $phppath = get_config('enrol_wisc', 'phppath');

        // Install a signal handler for SIGCHLD so that later when we call sleep() it be interrupted by
        // a child exiting.
        pcntl_signal(SIGCHLD, function() {}, true);

        reset($courses);
        while (current($courses) !== false || count($forks)) {

            while (count($forks) < $parallel && current($courses) !== false)
            {
                $courseid = current($courses);
                $coursename = $DB->get_field('course', 'shortname', array('id'=>$courseid), MUST_EXIST);
                mtrace("Executing backup for course $courseid - $coursename");

                self::start_automated_backup_course($courseid);

                if (($forks[$courseid] = pcntl_fork()) === 0) {
                    // Child, so execute the backup
                    $script = $CFG->dirroot."/local/uwmoodle/backuputil/cli/automated_backup_course.php";
                    $args = array($script, "--course=$courseid");
                    pcntl_exec($phppath, $args);
                    mtrace("Error executing $phppath");
                    // Shouldn't get here, unless something does wrong
                    posix_kill(getmypid(),9); // Commit suicide so that we don't shutdown the database connection.  Is there a better way to do this?
                    exit(0);   // this really shouldn't get run :)
                }
                next($courses); // advance to the next course
            }
            do {
                // Check if the jobs are still alive
                $status = 0;
                if ($pid = pcntl_wait($status, WNOHANG)) {
                    // Job has finished
                    $donecourseid = array_search($pid, $forks);
                    unset($forks[$donecourseid]);
                    $backupstatus = backup_cron_automated_helper::BACKUP_STATUS_ERROR;

                    if (pcntl_wifexited($status)) {
                        $exitstatus = pcntl_wexitstatus($status);
                        mtrace("Course $donecourseid backup exit status: ".$exitstatus);
                        if ($exitstatus == 0) {
                            // OK
                            $backupstatus = backup_cron_automated_helper::BACKUP_STATUS_OK;
                        } else if ($exitstatus == 2) {
                            // backup warning
                            $backupstatus = backup_cron_automated_helper::BACKUP_STATUS_WARNING;
                        } else {
                            // general error, exception, etc
                        }
                    } else if (pcntl_wifsignaled($status)) {
                        mtrace("Error: Course $donecourseid backup died on signal ".pcntl_wtermsig($status));
                    } else {
                        mtrace("Error: Course $donecourseid backup mysteriously died. (status = $status)");
                    }
                    self::finish_automated_backup_course($donecourseid, $backupstatus);
                } else {
                    // The pid was 0, so no child was available
                    //  Sleep for a while.  This will hopefully get interrupted by SIGCHLD.
                    // (Unless we got really unlucky and the SIGCHLD just came, in which case we might sleep for a while..)
                    sleep($SLEEPTIME);
                    // RT#326147 - We have to make some database call here, otherwise the mysql connection in the parent might timeout.
                    $unused = $DB->get_field('course', 'shortname', array('id'=>SITEID));
                }
            } while (count($forks) >= $parallel);
        }

        mtrace('Finished backup jobs');
    }

    /**
     * Update course_backup table at the start of a course backup
     *
     * @param int $courseid
     */
    protected static function start_automated_backup_course($courseid) {
        global $DB;
        // Mark backup as beginning
        $backupcourse = $DB->get_record('backup_courses', array('courseid'=>$courseid));
        if (!$backupcourse) {
            $backupcourse = new stdClass;
            $backupcourse->courseid = $courseid;
            $DB->insert_record('backup_courses',$backupcourse);
            $backupcourse = $DB->get_record('backup_courses', array('courseid'=>$courseid));
        }
        // Set laststarttime.
        $starttime = time();
        $backupcourse->laststarttime = time();
        $backupcourse->laststatus = backup_cron_automated_helper::BACKUP_STATUS_UNFINISHED;
        $DB->update_record('backup_courses', $backupcourse);
    }

    /**
     * Update course_backup table at the end of a course backup
     * @param ind $courseid
     * @param int $status
     */
    protected static function finish_automated_backup_course($courseid, $status) {
        global $DB;
        $backupcourse = $DB->get_record('backup_courses', array('courseid'=>$courseid), '*', MUST_EXIST);
        $nextstarttime = backup_cron_automated_helper::calculate_next_automated_backup(99, $backupcourse->laststarttime);

        $backupcourse->laststatus = $status;
        $backupcourse->lastendtime = time();
        $backupcourse->nextstarttime = $nextstarttime;

        $DB->update_record('backup_courses', $backupcourse);

        if ($backupcourse->laststatus === backup_cron_automated_helper::BACKUP_STATUS_OK) {
            // Clean up any excess course backups now that we have
            // taken a successful backup.
            $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
            $removedcount = backup_cron_automated_helper::remove_excess_backups($course);
        }
    }

    /**
     * Runs the automated backups if required
     *
     * This is a modified copy of backup_cron_automated_helper::run_automated_backup
     *
     * New changes there should probably be propagated here.
     *
     * @param int $parallel the number of backups to run at one time
     *
     * @global moodle_database $DB
     */
    public static function run_automated_backup($rundirective = backup_cron_automated_helper::RUN_ON_SCHEDULE, $parallel = 1) {
        global $CFG, $DB;

        $status = true;
        $emailpending = false;
        $now = time();
        $config = get_config('backup');

        $sitename = get_site()->shortname;
        mtrace("[$sitename] Checking automated backup status",'...');
        $state = backup_cron_automated_helper::get_automated_backup_state($rundirective);
        if ($state === backup_cron_automated_helper::STATE_DISABLED) {
            mtrace('INACTIVE');
            return $state;
        } else if ($state === backup_cron_automated_helper::STATE_RUNNING) {
            mtrace('RUNNING');
            if ($rundirective == backup_cron_automated_helper::RUN_IMMEDIATELY) {
                mtrace('Automated backups are already running. If this script is being run by cron this constitues an error. You will need to increase the time between executions within cron.');
            } else {
                mtrace("automated backup are already running. Execution delayed");
            }
            return $state;
        } else {
            mtrace('OK');
        }
        backup_cron_automated_helper::set_state_running();

        // Apply current backup version and release if necessary.  We do this here
        // since the actual backups are running from a read-only database.
        backup_controller_dbops::apply_version_and_release();

        mtrace("Getting admin info");
        $admin = get_admin();
        if (!$admin) {
            mtrace("Error: No admin account was found");
            $state = false;
        }

        if ($status) {
            mtrace("Checking courses");
            mtrace("Skipping deleted courses", '...');
            mtrace(sprintf("%d courses", backup_cron_automated_helper::remove_deleted_courses_from_schedule()));
        }

        if ($status) {

            mtrace('Running required automated backups...');

            // This could take a while!
            @set_time_limit(0);
            raise_memory_limit(MEMORY_EXTRA);

            $nextstarttime = backup_cron_automated_helper::calculate_next_automated_backup($admin->timezone, $now);
            $showtime = "undefined";
            if ($nextstarttime > 0) {
                $showtime = date('r', $nextstarttime);
            }

            $rs = $DB->get_recordset('course');
            foreach ($rs as $course) {
                $backupcourse = $DB->get_record('backup_courses', array('courseid'=>$course->id));
                if (!$backupcourse) {
                    $backupcourse = new stdClass;
                    $backupcourse->courseid = $course->id;
                    $DB->insert_record('backup_courses',$backupcourse);
                    $backupcourse = $DB->get_record('backup_courses', array('courseid'=>$course->id));
                }

                // The last backup is considered as successful when OK or SKIPPED.
                $lastbackupwassuccessful =  ($backupcourse->laststatus == backup_cron_automated_helper::BACKUP_STATUS_SKIPPED ||
                        $backupcourse->laststatus == backup_cron_automated_helper::BACKUP_STATUS_OK) && (
                                $backupcourse->laststarttime > 0 && $backupcourse->lastendtime > 0);

                // Skip courses that do not yet need backup.
                $skipped = !(($backupcourse->nextstarttime > 0 && $backupcourse->nextstarttime < $now) || $rundirective == backup_cron_automated_helper::RUN_IMMEDIATELY);
                $skippedmessage = 'Does not require backup';

                // If config backup_auto_skip_hidden is set to true, skip courses that are not visible.
                if (!$skipped && $config->backup_auto_skip_hidden) {
                    $skipped = ($config->backup_auto_skip_hidden && !$course->visible);
                    $skippedmessage = 'Not visible';
                }

                // If config backup_auto_skip_modif_days is set to true, skip courses
                // that have not been modified since the number of days defined.
                if (!$skipped && $lastbackupwassuccessful && $config->backup_auto_skip_modif_days) {
                    $timenotmodifsincedays = $now - ($config->backup_auto_skip_modif_days * DAYSECS);
                    $coursemodified = self::course_modified_since($course, $timenotmodifsincedays);
                    $skipped = !$coursemodified;
                    $skippedmessage = 'Not modified in the past '.$config->backup_auto_skip_modif_days.' days';
                }

                // If config backup_auto_skip_modif_prev is set to true, skip courses
                // that have not been modified since previous backup.
                if (!$skipped && $lastbackupwassuccessful && $config->backup_auto_skip_modif_prev) {
                    $coursemodified = self::course_modified_since($course, $backupcourse->laststarttime);
                    $skipped = !$coursemodified;
                    $skippedmessage = 'Not modified since previous backup';
                }

                // Skip courses not needed for backup.
                if ($skipped) {
                    // Output the next execution time when it has been updated.
                    if ($backupcourse->nextstarttime != $nextstarttime) {
                        mtrace('Backup of \'' . $course->shortname . '\' is scheduled on ' . $showtime);
                    }
                    $backupcourse->laststatus = backup_cron_automated_helper::BACKUP_STATUS_SKIPPED;
                    $backupcourse->nextstarttime = $nextstarttime;
                    $DB->update_record('backup_courses', $backupcourse);
                    mtrace('Skipping '.$course->shortname.' ('.$skippedmessage.')');
                } else {
                    // If the backup nextstarttime is in the future or 0, make it $now .
                    // We do this now so that nagios can pick up on failed backups by looking for backups
                    // with a nextstarttime in the past.
                    if ($backupcourse->nextstarttime == 0 || $backupcourse->nextstarttime > $now) {
                        $backupcourse->nextstarttime = $now;
                        $DB->update_record('backup_courses', $backupcourse);
                    }
                    // Backup every non-skipped courses.
                    mtrace('Backing up '.$course->shortname.'...', '');

                    // We have to send an email because we have included at least one backup.
                    $emailpending = true;

                    // queue for later processing
                    $coursestobackup[] = $course->id;

                    mtrace("queued");
                }
            }
            $rs->close();

            if (empty($coursestobackup)) {
                // Force a backup of course 1 so that nagios will be happy.
                mtrace('Forcing a backup of course id=1.');
                $coursestobackup[] = 1;
            }

            if (!empty($coursestobackup)) {
                self::run_backup_jobs($coursestobackup, $parallel);
            } else {
                mtrace('No courses to backup');
            }
        }

        //Send email to admin if necessary
        if ($emailpending) {
            mtrace("Sending email to admin");
            $message = "";

            $count = backup_cron_automated_helper::get_backup_status_array();
            $haserrors = ($count[backup_cron_automated_helper::BACKUP_STATUS_ERROR] != 0 || $count[backup_cron_automated_helper::BACKUP_STATUS_UNFINISHED] != 0);

            //Build the message text
            //Summary
            $message .= get_string('summary')."\n";
            $message .= "==================================================\n";
            $message .= "  ".get_string('courses').": ".array_sum($count)."\n";
            $message .= "  ".get_string('ok').": ".$count[backup_cron_automated_helper::BACKUP_STATUS_OK]."\n";
            $message .= "  ".get_string('skipped').": ".$count[backup_cron_automated_helper::BACKUP_STATUS_SKIPPED]."\n";
            if ($count[backup_cron_automated_helper::BACKUP_STATUS_ERROR]) {
                $message .= "  ".get_string('error').": ".$count[backup_cron_automated_helper::BACKUP_STATUS_ERROR]."\n";
            }
            if ($count[backup_cron_automated_helper::BACKUP_STATUS_UNFINISHED]) {
                $message .= "  ".get_string('unfinished').": ".$count[backup_cron_automated_helper::BACKUP_STATUS_UNFINISHED]."\n";
            }
            if ($count[backup_cron_automated_helper::BACKUP_STATUS_WARNING]) {
                $message .= "  ".get_string('warning').": ".$count[backup_cron_automated_helper::BACKUP_STATUS_WARNING]."\n\n";
            }

            //Reference
            if ($haserrors) {
                $message .= "  ".get_string('backupfailed')."\n\n";
                $dest_url = "$CFG->wwwroot/report/backups/index.php";
                $message .= "  ".get_string('backuptakealook','',$dest_url)."\n\n";
                //Set message priority
                $admin->priority = 1;
                //Reset unfinished to error
                $DB->set_field('backup_courses','laststatus','0', array('laststatus'=>'2'));
            } else {
                $message .= "  ".get_string('backupfinished')."\n";
            }

            // Write summary to output
            mtrace($message);
        }

        //Everything is finished stop backup_auto_running
        backup_cron_automated_helper::set_state_running(false);

        mtrace('Automated backups complete.');

        return $status;
    }

    /**
     * Run a single automated course backup
     *
     * This code is mostly copied from backup_cron_automated_helper::run_automated_backup
     * New changes there should probably be propagated here.
     *
     * @param int $courseid the moodle courseid
     * @return int backup status code
     */
    public static function run_automated_course_backup($courseid) {
        global $CFG, $DB;

        mtrace("Backing up course $courseid");

        // The following was removed due to conflicts with standard slave backups.  RT #96715
        /*
        if (defined('UWMOODLE_AUTOMATED_BACKUPS_FROM_SLAVE')) {
            // Ensure that slave is running
            $res = $DB->get_record_sql("SHOW GLOBAL STATUS LIKE 'Slave_running'");
            if (!$res) {
                mtrace("WARNING: Unable to query mysql slave status.");
                return backup_cron_automated_helper::BACKUP_STATUS_WARNING;
            }
            if ($res->value != "ON") {
                mtrace("ERROR: Mysql slave not running.  Aborting.");
                return backup_cron_automated_helper::BACKUP_STATUS_ERROR;
            }
            mtrace("Slave status OK");
        }
        */

        //mtrace("Getting admin info");
        $admin = get_admin();

        $course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);

        // Don't try to write to the mdl_backup_log table.  We'll let the errors go to syslog.
        $CFG->backup_database_logger_level = backup::LOG_NONE;

        $backupstatus = self::launch_automated_backup($course, $admin->id);

        mtrace("Finished course $courseid");

        return $backupstatus;
    }

    /**
     * Launches a automated backup routine for the given course
     *
     * This code is mostly copied from backup_cron_automated_helper::launch_automated_backup
     * New changes there should probably be propagated here.
     *
     * The main difference is that here we exclude logs from the site course.
     *
     * @param stdClass $course
     * @param int $userid
     * @return int backup status
     */
    public static function launch_automated_backup($course, $userid) {
        global $SITE;
        $outcome = backup_cron_automated_helper::BACKUP_STATUS_OK;
        $config = get_config('backup');
        $bc = new backup_controller(backup::TYPE_1COURSE, $course->id, backup::FORMAT_MOODLE, backup::INTERACTIVE_NO, backup::MODE_AUTOMATED, $userid);

        try {

            $settings = array(
                    'users' => 'backup_auto_users',
                    'role_assignments' => 'backup_auto_role_assignments',
                    'activities' => 'backup_auto_activities',
                    'blocks' => 'backup_auto_blocks',
                    'filters' => 'backup_auto_filters',
                    'comments' => 'backup_auto_comments',
                    'completion_information' => 'backup_auto_userscompletion',
                    'logs' => 'backup_auto_logs',
                    'histories' => 'backup_auto_histories'
            );
            foreach ($settings as $setting => $configsetting) {
                if ($bc->get_plan()->setting_exists($setting)) {
                    $bc->get_plan()->get_setting($setting)->set_value($config->{$configsetting});
                }
            }

            // Exclude logs for the Site course, as those logs are enormous
            if ($course->id == $SITE->id) {
                $bc->get_plan()->get_setting('logs')->set_value(0);
            }

            // Set the default filename
            $format = $bc->get_format();
            $type = $bc->get_type();
            $id = $bc->get_id();
            $users = $bc->get_plan()->get_setting('users')->get_value();
            $anonymised = $bc->get_plan()->get_setting('anonymize')->get_value();
            $bc->get_plan()->get_setting('filename')->set_value(backup_plan_dbops::get_default_backup_filename($format, $type, $id, $users, $anonymised));

            $bc->set_status(backup::STATUS_AWAITING);

            $bc->execute_plan();
            $results = $bc->get_results();
            $outcome = backup_cron_automated_helper::outcome_from_results($results);
            $file = (!empty($results['backup_destination']))?$results['backup_destination']:''; // may be empty if file already moved to target location
            $dir = $config->backup_auto_destination;
            $storage = (int)$config->backup_auto_storage;
            if (!file_exists($dir) || !is_dir($dir) || !is_writable($dir)) {
                $dir = null;
            }
            //// Skip the zip copy since we're doing rsync backups
//             if ($file && !empty($dir) && $storage !== 0) {
//                 $filename = backup_plan_dbops::get_default_backup_filename($format, $type, $course->id, $users, $anonymised, !$config->backup_shortname);
//                 if (!$file->copy_content_to($dir.'/'.$filename)) {
//                     $outcome = backup_cron_automated_helper::BACKUP_STATUS_ERROR;
//                 }
//                 if ($outcome != backup_cron_automated_helper::BACKUP_STATUS_ERROR && $storage === 1) {
//                     $file->delete();
//                 }
//             }

        } catch (moodle_exception $e) {
            $bc->log('backup_auto_failed_on_course', backup::LOG_ERROR, $course->shortname); // Log error header.
            $bc->log('Exception: ' . $e->errorcode, backup::LOG_ERROR, $e->a, 1); // Log original exception problem.
            $bc->log('Debug: ' . $e->debuginfo, backup::LOG_DEBUG, null, 1); // Log original debug information.
            $outcome = backup_cron_automated_helper::BACKUP_STATUS_ERROR;
        }

        $bc->destroy();
        unset($bc);

        return $outcome;
    }

    public static function course_modified_since($course, $time) {
        global $DB;

        if (function_exists('get_log_manager')) {
            // Moodle >= 2.7
            $logmang = get_log_manager();
            $readers = $logmang->get_readers('core\log\sql_reader');
            if (empty($readers)) {
                // Moodle 2.7,2.8
                $readers = $logmang->get_readers('core\log\sql_select_reader');
            }
            if (empty($readers)) {
                mtrace("Warning: No logstore readers.  Unable to check for course changes.");
            }
            $where = "courseid = :courseid and timecreated > :since and crud <> 'r'";
            $params = array('courseid' => $course->id, 'since' => $time);
            foreach ($readers as $reader) {
                if ($reader->get_events_select_count($where, $params)) {
                    return true;
                }
            }
            return false;
        }

        // Moodle < 2.7

        $result = $course->timemodified >= $time;
        if (!$result) {
            // Check log if there were any modifications to the course content.
            $sqlwhere = "course=:courseid
                         AND time>=:time
                         AND ".$DB->sql_like('action', ':actionview', false, true, true)
                      ." AND ".$DB->sql_like('action', ':actionreport', false, true, true);
            $params = array('courseid' => $course->id,
                    'time' => $time,
                    'actionview' => '%view%',
                    'actionreport' => '%report%');
            $result = $DB->record_exists_select('log', $sqlwhere, $params);
        }
        if (!$result) {
            // Check role assignments to see if any new editinginstructors were assigned.
            // We check this to ensure that the instructor information is up to date for the local/archive plugin.
            $coursecontext = context_course::instance($course->id);
            $teacherroleid = $DB->get_field('role', 'id', array('shortname'=>'editingteacher'), IGNORE_MISSING);
            if ($teacherroleid !== false) {
                $sqlwhere = "roleid=:roleid
                             AND contextid=:contextid
                             AND timemodified>=:time";
                $params = array('roleid'=>$teacherroleid, 'contextid'=>$coursecontext->id, 'time'=>$time);
                $result = $DB->record_exists_select('role_assignments', $sqlwhere, $params);
            }
        }
        return $result;
    }
}
