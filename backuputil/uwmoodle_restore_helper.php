<?php

/**
 * Utility helper for restores run through the cli.
 */

require_once($CFG->libdir.'/grouplib.php');
require_once($CFG->dirroot.'/group/lib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/enrol/wisc/controller_helper.class.php');
require_once($CFG->dirroot.'/enrol/wisc/ra_datastore.class.php');

/**
 * This class is an abstract class with methods that can be called to aid the
 * running of restores from the cli.
 */
abstract class uwmoodle_restore_helper {

    /**
     * Runs the restore
     *
     * @global moodle_database $DB
     */
    public static function run_restore($options) {
        global $DB;

        mtrace("Running restore of " . $options['backup'] . "...");

        // This could take a while!
        @set_time_limit(0);
        raise_memory_limit(MEMORY_EXTRA);

        uwmoodle_restore_helper::launch_restore($options['backup'], $options);
        mtrace('Restore complete.');
    }



    /**
     * Launches restore for the given course
     *
     * @return bool
     */
    public static function launch_restore($backupfile, $options) {
        global $USER, $CFG, $DB;

        $CFG->keeptempdirectoriesonbackup = true;  // HACK: keep the temp directory around.  There is a better way

        $backupsettings = array(
                'activities' => 1,
                'blocks' => 1,
                'filters' => 1,
                'users' => $options['users'],
                'role_assignments' => 1,
                'calendarevents' => 1,
                'comments' => 1,
                'userscompletion' => $options['users'],
                'logs' => $options['users'],
                'grade_histories' => $options['users'],  // >= 2.6
                'enrol_migratetomanual' => $options['manualenrol'],
        );

        if (!is_dir("$CFG->tempdir/backup")) {
            mkdir("$CFG->tempdir/backup",$CFG->directorypermissions);
        }

        $options['filepath'] = restore_controller::get_tempdir_name($backupfile, $USER->id);
        $options['tempdestination'] = "$CFG->tempdir/backup/" . $options['filepath'];

        if (is_dir($backupfile)) {
            self::copyr($backupfile, $options['tempdestination']);
        } else {
            echo "Unzipping";
            $fp = get_file_packer('application/vnd.moodle.backup');
            $unzipresult = $fp->extract_to_pathname($backupfile, $options['tempdestination']);
        }

        if($options['anonymize']){
            self::anonymize_users($options);
        }

        $backupsettings['enrol_migratetomanual'] = $options['manualenrol'];

        // Create new course.
        list($fullname, $shortname) = restore_dbops::calculate_course_names(0, get_string('restoringcourse', 'backup'), get_string('restoringcourseshortname', 'backup'));

        // Decide what category to put it in
        $category = null;
        if ($options['category']) {
            // if a categoryid was supplied in cli call, attempt to get the category
            $category = coursecat::get($options['category'], MUST_EXIST);
        }
        if (!$category) {
            // otherwise try to use the same category as the backup course.  This will only work for
            // UW Moodle backups
            $categoryid = self::get_restore_category($options);
            if ($categoryid) {
                $category = coursecat::get($categoryid, IGNORE_MISSING);
            }
        }
        if (!$category) {
            // otherwise use the default category
            $category = coursecat::get(get_config("enrol_wisc", "category"), MUST_EXIST);
        }

        // use the default course creation category for staging the restore
        $tempcategory = coursecat::get(get_config("enrol_wisc", "tempcategory"), MUST_EXIST);

        $newcourseid = restore_dbops::create_new_course($fullname, $shortname, $tempcategory->id);

        $rc = new restore_controller($options['filepath'], $newcourseid,
                backup::INTERACTIVE_NO, backup::MODE_GENERAL, $USER->id, backup::TARGET_NEW_COURSE);

        foreach ($backupsettings as $name => $value) {
            if ($rc->get_plan()->setting_exists($name)) {
                $setting = $rc->get_plan()->get_setting($name);
                $setting->set_value($value);
            } else {
                echo "Warning, no setting: $name\n";
            }
        }

        if (!$rc->execute_precheck()) {
            $precheckresults = $rc->get_precheck_results();
            if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
                fulldelete($options['tempdestination']);

                echo "===PRECHECK ERRORS===\n";
                foreach ($precheckresults['errors'] as $error) {
                    echo "ERROR:" . $error . "\n";
                }

                throw new moodle_exception('backupprecheckerrors', 'webservice');
            }
            if (is_array($precheckresults) && array_key_exists('warnings', $precheckresults)) {
                echo "===PRECHECK WARNINGS===\n";
                foreach ($precheckresults['warnings'] as $warning) {
                    echo "WARNING:" . $warning . "\n";
                }
            }
        }

        $rc->execute_plan();
        $rc->destroy();
        if(!$options['manualenrol'] && $options['users']){
            self::restore_wisc_enrol($newcourseid, $options);
        }

        // Delete the temp directory now
        fulldelete($options['tempdestination']);

        // Move the course to the correct category
        move_courses(array($newcourseid), $category->id);
    }

    /**
     * Copy a file, or recursively copy a folder and its contents
     *
     * @author      Aidan Lister <aidan@php.net>
     * @version     1.0.1
     * @link        http://aidanlister.com/repos/v/function.copyr.php
     * @param       string   $source    Source path
     * @param       string   $dest      Destination path
     * @return      bool     Returns TRUE on success, FALSE on failure
     */
    public static function copyr($source, $dest)
    {
        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            if ($dest !== "$source/$entry") {
                self::copyr("$source/$entry", "$dest/$entry");
            }
        }

        // Clean up
        $dir->close();
        return true;
    }

    public static function restore_wisc_enrol($newcourseid, $options) {
        global $USER, $CFG, $DB;

        mtrace('Looking for section groupings');
        $groupings = groups_get_all_groupings($newcourseid);
        foreach ($groupings as $grouping) {
            if (preg_match('/ Sections$/', $grouping->name)) { // We'll assume these are ours to delete.
                                                               //  They will be regenerated when the sync is run
                mtrace('Deleting grouping: '.$grouping->name);
                $members = $DB->get_records('groupings_groups', array('groupingid'=>$grouping->id));
                foreach ($members as $member) {
                    mtrace('Deleting group id '.$member->groupid);
                    groups_delete_group($member->groupid);
                }
                groups_delete_grouping($grouping->id);
            }
        }


        mtrace('Updating wisc enrollments');
        //$enrolfile = "$CFG->dataroot/temp/backup/$filepath/course/enrolments.xml";
        $enrolfile = $CFG->tempdir . '/backup/' . $options['filepath'] . '/course/enrolments.xml';
        if (!file_exists($enrolfile)) {
            mtrace("Unable to find $enrolfile");
            return;
        }
        $simple = simplexml_load_file( $enrolfile );
        if (!$simple) {
            mtrace("Unable to load $enrolfile");
        }

        $subjects = array();
        $term = null;
        $enrols = $simple->enrols;
        if (!empty($enrols->enrol)) {
            foreach ($enrols->enrol as $enrolment) {
                if(!($sections = $enrolment->plugin_enrol_wisc_enrol)){
                    continue;
                }

                // add wisc/enrol instance
                $plugin = enrol_get_plugin('wisc');
                $fields = array();
                if ($enrolment->{enrol_wisc_plugin::termfield} != '$@NULL@$') {
                    $fields[enrol_wisc_plugin::termfield] = (string) $enrolment->{enrol_wisc_plugin::termfield};
                }
                if ($enrolment->{enrol_wisc_plugin::tarolefield} != '$@NULL@$') {
                    $fields[enrol_wisc_plugin::tarolefield] = (string) $enrolment->{enrol_wisc_plugin::tarolefield};
                }
                $course = $DB->get_record('course', array('id'=>$newcourseid));
                $instanceid = $plugin->add_instance($course, $fields);
                $instance = $DB->get_record('enrol', array('courseid'=>$newcourseid, 'enrol'=>'wisc', 'id'=>$instanceid), '*', MUST_EXIST);

                $term = (string) $enrolment->{enrol_wisc_plugin::termfield};
                foreach($sections->coursemaps->coursemap as $section){
                    $coursemap = new stdClass();
                    foreach ($section as $key=>$value) {
                        if (((string)$value) != '$@NULL@$') {
                            $coursemap->{(string)$key} = (string)$value;
                        }
                    }
                    $coursemap->courseid = $newcourseid;
                    $coursemap->enrolid = $instanceid;
                    $coursemap->groupid = null; // will be redone on sync
                    $DB->insert_record('enrol_wisc_coursemap', $coursemap);
                    $subjects[$coursemap->subject_code] = $coursemap->subject_code;
                }

                try {
                    $wisc = enrol_get_plugin('wisc');
                    $wisc->sync_instance($instance, $course);
                } catch (Exception $e) {
                    mtrace("Error syncing course enrollments: ".$e->getMessage());
                }
            }
        }
    }

    public static function get_restore_category($options) {
        global $USER, $CFG, $DB;

        mtrace('Updating category');
        $coursefile = $CFG->tempdir . '/backup/' . $options['filepath'] . '/course/course.xml';
        if (!file_exists($coursefile)) {
            mtrace("Unable to find $coursefile");
            return;
        }
        $simple = simplexml_load_file( $coursefile );
        if (!$simple) {
            mtrace("Unable to load $coursefile");
        }

        $parentcategory = $simple->plugin_local_archive_course->parentcategories->parentcategory[0];
        if (!$parentcategory) {
            mtrace("No category information found");
            return;
        }

        $catidnumber = (string) $parentcategory->idnumber;

        return enrol_wisc_controller_helper::create_category_by_idnumber($catidnumber);

    }


    /**
     * Anonymize user data
     *
     * @param array $options CLI options
     */
    public static function anonymize_users($options){

        mtrace('Anonymizing user data');
        $userfile = $options['tempdestination'] . "/users.xml";
        if (!file_exists($userfile)) {
            mtrace("Unable to find $userfile");
            return;
        }
        $simple = simplexml_load_file( $userfile );
        if (!$simple) {
            mtrace("Unable to load $userfile");
        }
        // Get and anonymize users
        $counter = 1; // Counter for the user we're on
        $users = $simple->user;
        foreach($users as $user){
            $user->username = 'anon' . $counter;
            $user->idnumber = '';
            $user->firstname = 'anonfirstname' . $counter;
            $user->lastname = 'anonlastname' . $counter;
            $user->email = 'anon' . $counter . '@doesntexist.com';
            $user->skype = '';
            $user->yahoo = '';
            $user->aim = '';
            $user->msn = '';
            $user->phone1 = '';
            $user->phone2 = '';
            $user->institution = '';
            $user->department = '';
            $user->address = '';
            $user->city = '';
            $user->country = '';
            $user->picture = 0;
            $user->description = '';
            $counter++;
        }
        mtrace('Finished changing users...' . ($counter - 1) );
        //Rename old file
        rename($options['tempdestination'] . '/users.xml' , $options['tempdestination'] . '/users.bak.xml');
        if($simple->asXML($options['tempdestination'] . '/users.xml')){
            mtrace('New user file written successfully');
        }else{
            mtrace('New user file error');
        }
    }

    /**
     * Convert backups to moodle2 format.
     *
     * Note: The input and output will be unzipped
     *
     * @param string $backup  The backup directory
     * @param string $outputdir  Where to place the generated backup
     * @return bool
     */
    public static function convert($backup, $outputdir) {
        global $CFG;
        require_once($CFG->dirroot . '/backup/util/helper/convert_helper.class.php');

        $tempdir = restore_controller::get_tempdir_name();
        $tempdirpath = "$CFG->tempdir/backup/" . $tempdir;

        self::copyr($backup, $tempdirpath);

        $logger = new error_log_logger(backup::LOG_DEBUG);
        $result = convert_helper::to_moodle2_format($tempdir, null, $logger);

        if ($result) {
            self::copyr($tempdirpath, $outputdir.'/'.basename($backup).'-moodle2');
        }
        fulldelete($tempdirpath);

        return $result;
    }

    /**
     * Convert user pictures from backup into a directory for upload to a new site.
     *
     * Note: The input and output will be unzipped
     *
     * @param string $backup  The backup directory
     * @param string $outputdir  Where to place the generated images
     * @return bool
     */
    public static function convert_user_pictures($backup, $outputdir) {
        global $CFG;

        // Load files.xml
        $filesxmlfile = $backup.'/files.xml';
        $filesxml = simplexml_load_file($filesxmlfile);
        if (!$filesxml) {
            throw new moodle_exception('Cannot access \''.$filesxmlfile.'\'');
        }
        // Make a hash of all files in course.
        $filehash = array();
        foreach($filesxml->file as $file) {
            $attrs = $file->attributes();
            $filehash[(string)($attrs['id'])] = (string)$file->contenthash;
        }

        // Load users file.
        $usersfile = $backup.'/users.xml';
        $usersxml = simplexml_load_file($usersfile);
        if (!$usersxml) {
            throw new moodle_exception('Cannot access \''.$usersfile.'\'');
        }

        // Loop through users and copy pictures.
        $counter = 0; // number of images counter
        foreach($usersxml->user as $user){
            $picture = (integer)$user->picture;
            if ($picture != 0) {
                if (!isset($filehash[$picture])) {
                    mtrace('Missing file record for '.$user->username .' ('.$picture.') ');
                    continue;
                }
                $contenthash = $filehash[$picture];
                $picturepath = $backup . '/files/' . substr($contenthash, 0, 2) . '/' . $contenthash;
                validate_param($contenthash, PARAM_FILE);  //throws exception on invalid
                validate_param((string) $user->username, PARAM_FILE);  //throws exception on invalid
                $destpath = $outputdir . '/' . $user->username . '.png';
                if (!copy($picturepath, $destpath)) {
                    mtrace('Unable to copy image file for '.$user->username);
                } else {
                    $counter++;
                }
            }
        }
        mtrace('Processed ' . $counter . ' users.' );

        return true;
    }
}
