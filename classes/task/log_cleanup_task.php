<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Standard log cleanup task
 *
 * @package    local_uwmoodle
 * @copyright  2015 University of Wisconsin - Madison
 * @author     Matt Petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_uwmoodle\task;

defined('MOODLE_INTERNAL') || die();

class log_cleanup_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return "UW Moodle log cleanup task";
    }

    /**
     * Remove webservice and nagios records from the logs table.
     *
     * These rows account for the majority of log entries on older sites, and
     * serve no pedagogical value.
     */
    public function execute() {
        global $DB,$CFG;
        
        $WEBSERVICE_RETENTION = 15; // in days
        $NAGIOS_RETENTION = 7; // in days
        $ANON_RETENTION = 15;
 
        $SERVER_NETS = array(
	    '2607:f388:1082:3:%',
	    '2607:f388:1082:2:%',
	    '2607:f388:1082:1:%',
	    '2607:f388:1082:0:%',
	    '2607:f388:1082:ff:%',
            '2607:f388:1082:ffff:%',
            '128.104.196.%',
            '128.104.197.%',
	    '128.104.201.%',
	    '144.92.12.%',
	    '144.92.13.%',
	    '128.104.203.%',
        );
        
        $now = time();

        // Get last run time
        $lastrun = get_config('local_uwmoodle', 'last_log_cleanup');
        if ($lastrun === false) {
            $lastrun = 0;
        }

        // How long ago were the logs last purged?  No sense in looking at records we've
        // already processed.
        $window = $now - $lastrun + 1;

        // Purge Webservice records
        $webservicetime = $now - ($WEBSERVICE_RETENTION * 3600 * 24);
        $select = "timecreated < ? AND timecreated >= ? AND origin='ws'";
        mtrace(" UW Moodle: Deleting old webservice records from standard log store.");
        $DB->delete_records_select("logstore_standard_log", $select, array($webservicetime, $webservicetime - $window));

        // Purge nagios records
        $nagiosuser = $DB->get_record('user', array('username' => 'nagios', 'mnethostid'=>$CFG->mnet_localhost_id));
        if ($nagiosuser) {
            $nagiostime = $now - ($NAGIOS_RETENTION * 3600 * 24);
            $select = "timecreated < ? AND timecreated >= ? AND userid=?";
            mtrace(" UW Moodle: Deleting old nagios records from standard log store.");
            $DB->delete_records_select("logstore_standard_log", $select, array($nagiostime, $nagiostime - $window, $nagiosuser->id));
        } else {
            mtrace(" No nagios user, skipping log cleanup.");
        }
        
        // Purge anonymous accesses from CAE server nets
        $anonservicetime = $now - ($ANON_RETENTION * 3600 * 24);
        $netsql = join(' OR ', array_fill(0, count($SERVER_NETS), 'ip LIKE ? '));
        $select = "timecreated < ? AND timecreated >= ? AND userid=0 AND crud='r' AND ( $netsql )";
        mtrace(" UW Moodle: Deleting nagios/collectd access from standard log store.");
        $DB->delete_records_select("logstore_standard_log", $select, array_merge( array($anonservicetime, $anonservicetime - $window), $SERVER_NETS));

        set_config ('last_log_cleanup', $now, 'local_uwmoodle');
    }
}
