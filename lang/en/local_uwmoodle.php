<?php
$string['pluginname'] = 'UWMoodle CLI Utilities';
$string['sessionuserlockerr'] = 'Too many session locks.<br />Wait for your current requests to finish and try again later.';
$string['sessionopenerror'] = 'Unable to connect to session database.';
$string['hostkeyverifyfailed'] = 'Unable to verify hostkey.';