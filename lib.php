<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * UW Moodle Utils lib file
 *
 * @package    local
 * @subpackage uwmoodle
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Cron job to remove webservice login/logout records from the mdl_logs table.
 * These rows account for the majority of log entries on older sites, and
 * serve no pedagogical value.  This function does nothing on moodle >= 2.7
 *
 * {@see \local_uwmoodle\task\log_cleanup_task} for Moodle 2.7+ version.
 */
function local_uwmoodle_cron() {
    global $DB, $CFG;

    if ($CFG->version >= 2014051200) {
        // For moodle >= 2.7, the task API version of this code will run instead.
        return;
    }

    ////////////////////////////////////////
    // THIS CODE IS FOR MOODLE <= 2.6 ONLY//
    ////////////////////////////////////////

    // Ensure that this script only runs once a day, at 4am
    $lastrun = get_config('local_uwmoodle', 'lastlogclean');
    if (!$lastrun) {
        $lastrun = 0;
    }
    $now = time();
    // Once every 24 hours.
    if (time() - $lastrun < 24*60*60) {
        return;
    }
    // Only between 4 and 5 am.
    $start = strtotime(date('d-M-Y', $now)) + 4*60*60;
    $end = $start + 60*60;
    if ($now < $start || $now > $end) {
        return;
    }
    set_config('lastlogclean', $now, 'local_uwmoodle');


    $WEBSERVICE_RETENTION = 15; // in days
    $NAGIOS_RETENTION = 7; // in days

    $wsusers = $DB->get_records_menu('user', array('auth'=>'webservice'));
    if (!empty($wsusers)) {
        list ($usersql, $userparams) = $DB->get_in_or_equal(array_keys($wsusers), SQL_PARAMS_NAMED);

        mtrace("UW Moodle: Deleting old webservice login/logout records.");
        $userparams['time'] = time() - ($WEBSERVICE_RETENTION * 3600 * 24);
        $DB->delete_records_select('log', "userid $usersql AND ( action IN ('login','logout') OR module = 'webservice' ) AND time < :time", $userparams);
    }

    $nagiosuser = $DB->get_record('user', array('username' => 'nagios', 'mnethostid'=>$CFG->mnet_localhost_id));
    if ($nagiosuser) {
        $nagiostime = time() - ($NAGIOS_RETENTION * 3600 * 24);
        $select = "time < ? AND userid = ?";
        mtrace("UW Moodle: Deleting old nagios records logs.");
        $DB->delete_records_select("log", $select, array($nagiostime, $nagiosuser->id));
    } else {
        mtrace(" No nagios user, skipping log cleanup.");
    }
}
