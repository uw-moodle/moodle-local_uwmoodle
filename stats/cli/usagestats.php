<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Output csv usage log to stdout.
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/stats/uwmoodle_stats_helper.php');


if (moodle_needs_upgrading()) {
    cli_error("Moodle upgrade pending, backup execution suspended.\n");
    exit(1);
}

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
        'quiet'=>false, 'start'=>false, 'end'=>false, 'threshold'=>1),
        array('h'=>'help',
              'q'=>'quiet',
              's'=>'start',
              'e'=>'end',
              't'=>'threshold',
        ));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['start']) {
    $help =
    "Generate usage logs

Options:
-h, --help            Print out this help
-q, --quiet           Don't print status to stderr
-s, --start	          Start time (mandatory)  -- see strtotime()
-e, --end             End time  -- see strtitime()
-t, --threshold       Omit courses with < this number of users active in an hour.

Example:  sudo -u www-data php usagestats.php --start='sep 6' --end='sep 13'
          sudo -u www-data php usagestats.php --start='last sunday' --end='now'
";

    mtrace($help);
    exit(1);
}

$quiet = $options['quiet'];
$start = $options['start'];
$end = $options['end'];
$threshold = $options['threshold'];

$starttime = microtime();

if ($end) {
    $end = strtotime($end);
    if (!$end) {
        cli_error("Invalid end time");
        exit(1);
    }
} else {
    $end = time();
}

$start = strtotime($start);
if (!$start) {
    cli_error("Invalid start time");
    exit(1);
}

/// Start output log
$timenow = time();

if (!$quiet) {
    fwrite(STDERR,"Server Time: ".date('r',$timenow)."\n");
    fwrite(STDERR,"Start time: ".date('d M Y H:i:s', $start)."\n");
    fwrite(STDERR,"End time: ".date('d M Y H:i:s', $end)."\n");

}

// generate the stats
uwmoodle_stats_helper::generate_usage_stats($start, $end, $threshold, $quiet);

if (!$quiet) {
    fwrite(STDERR,"\nCompleted\n");

    $difftime = microtime_diff($starttime, microtime());
    fwrite(STDERR,"Execution took ".$difftime." seconds\n");
}