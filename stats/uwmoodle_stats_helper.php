<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * University of Wisconsin Moodle stats functions
 *
 * @package    local
 * @subpackage wiscservices
 * @copyright  2015 University of Wisconsin
 * @author     Matt petro
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Abstract class with static methods
 */
abstract class uwmoodle_stats_helper {

    /**
     * Output stats in csv format to stdout.
     *
     * @param int $start unix timestamp
     * @param int $end unix timestamp
     * @param int $threshold min number of users in course per interval
     * @param bool $quiet don't print status to STDERR
     */
    public static function generate_usage_stats($start, $end, $threshold = 1, $quiet = true) {
        global $DB;

        $resolution = 60*60;  // Stats resolution in seconds.

        $rs = $DB->get_recordset_select('logstore_standard_log', 'timecreated >= :start AND timecreated <= :end',
                        array('start'=>$start, 'end'=>$end), 'id');

        $data = array();
        $intervalstart = $start;

        self::output_stats_header();

        foreach ($rs as $log) {
            // Skip to next interval if necessary
            while (($log->timecreated - $intervalstart) >= $resolution) {
                self::output_stats_row($intervalstart, $data, $threshold);
                $intervalstart += $resolution;
                $data = array();
                if (!$quiet) {
                    fwrite(STDERR, "."); fflush(STDERR);
                }
            }

            if ($log->origin != 'web') {
                // ignore stuff other than regular access from the web
            }

            if (!isset($data[$log->courseid])) {
                $data[$log->courseid] = array('users'=>array(), 'logcount'=>0);
            }

            $data[$log->courseid]['users'][$log->userid] = true;
            $data[$log->courseid]['logcount'] += 1;
        }
        self::output_stats_row($intervalstart, $data, $threshold);

        $rs->close();

    }

    public static function output_stats_header() {
        echo "timestamp,week,dayofweek,hour,url,shortname,users,logcount\n";
    }

    public static function output_stats_row($intervalstart, &$data, $threshold) {
        global $CFG, $DB;

        static $shortnamecache;

        if (!$shortnamecache) {
            $shortnamecache = $DB->get_records_menu('course', array(), '', 'id,shortname');
        }

        $timeoutput = date("U,W,N,G",$intervalstart);
        foreach ($data as $courseid=>$stats) {
            if ($courseid == 0 || $courseid == SITEID) {
                // ignore site and non-course access.
                continue;
            }
            if (!isset($shortnamecache[$courseid])) {
                // course doesn't exist anymore?
                continue;
            }
            $url = $CFG->wwwroot."/course/view.php?id=$courseid";
            $shortname = str_replace('"','""', $shortnamecache[$courseid]);
            $numusers = count($stats['users']);
            $logcount = $stats['logcount'];
            if ($numusers < $threshold) {
                // Too few users
                continue;
            }
            echo "$timeoutput,\"$url\",\"$shortname\",$numusers,$logcount\n";
        }
    }
}
