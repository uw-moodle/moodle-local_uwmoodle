<?php

/**
 * Create anon users anon1,anon2,anon3, etc. which are used for restoring courses with anonymized backups.
 * We prepopulate these users so that instructors can restore anonymized backups without needing access
 * to create users during restore.  It's safe to call this script multiple times, as we check to make sure
 * the accounts don't exist before creating them.
 *
 * @author Matt Petro
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/util/uwmoodle_util_helper.php');      // cli only functions


if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
        'count'=>false),
        array('h'=>'help',
              'c'=>'count'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['count']) {
    $help =
    "Create moodle anon users.  This ensures that users anon1, anon2, ... exist.

Options:
-h, --help            Print out this help
-c=N, --count=N       Number of anon users.

";

    echo $help;
    die;
}

$count = $options['count'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n");

// Purge the logs
uwmoodle_util_helper::create_anon_users($count);

mtrace("\n\nCompleted");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");