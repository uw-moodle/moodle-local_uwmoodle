<?php

/**
 * Bulk enrol users in a course
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/util/uwmoodle_util_helper.php');      // cli only functions


if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

// now get cli options
list($options, $usernames) = cli_get_params(array('help'=>false,
        'course'=>false, 'role'=>false),
        array('h'=>'help',
              'c'=>'course',
              'r'=>'role'));

if ($options['help'] || !$options['course'] || !$options['role']) {
    $help =
    "Bulk enrol users in a course.

php enrolstudents.php --course=CF101 --role=student [username|netid@wisc.edu] ...

Options:
-h, --help            Print out this help
-c, --course          Moodle course shortname
-r, --role            Moodle role shortname'
";

    echo $help;
    die;
}

$course = $options['course'];
$role = $options['role'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n\n");

uwmoodle_util_helper::enrol_users($course, $role, $usernames);

mtrace("Completed");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");