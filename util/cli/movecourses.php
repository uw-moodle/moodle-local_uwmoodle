<?php

/**
 * Bulk move courses to a single category
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/util/uwmoodle_util_helper.php');      // cli only functions


if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

// now get cli options
list($options, $courses) = cli_get_params(array('help'=>false,
        'category'=>false),
        array('h'=>'help',
              'c'=>'category',
              'r'=>'role'));

if ($options['help'] || !$options['category']) {
    $help =
    "Bulk move courses to a new category.

php movecourses.php --category=3  courseid1 courseid2 ...

Options:
-h, --help            Print out this help
-c, --category        Moodle categoryid
";

    echo $help;
    die;
}

$category = $options['category'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n\n");

uwmoodle_util_helper::move_courses($courses, $category);

mtrace("Completed");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");