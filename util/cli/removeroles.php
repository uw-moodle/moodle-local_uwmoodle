<?php

/**
 * Remove redundant roles from category managers
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/util/uwmoodle_util_helper.php');      // cli only functions


if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
        'user'=>false),
        array('h'=>'help',
              'u'=>'user'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['user']) {
    $help =
    "Remove redundant roles from category managers.

Options:
-h, --help            Print out this help
-u, --user            Moodle user id

";

    echo $help;
    die;
}

$userid = $options['user'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n\n");

// Run automated backups if required.
uwmoodle_util_helper::fix_roles($userid);

mtrace("Completed");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");