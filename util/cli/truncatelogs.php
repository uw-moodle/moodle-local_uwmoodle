<?php

/**
 * Truncate moodle log tables of any row before a given date.  The operation is done slowly so
 * that it doesn't hang up the site when deleting millions of rows.
 *
 * The tables truncated are:
 *
 *   log
 *   grade_outcomes_history
 *   grade_categories_history
 *   grade_items_history
 *   grade_grades_history
 *   scale_history
 */

define('CLI_SCRIPT', true);

require(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/config.php');
require_once($CFG->libdir.'/clilib.php');      // cli only functions
require_once($CFG->dirroot.'/local/uwmoodle/util/uwmoodle_util_helper.php');      // cli only functions


if (moodle_needs_upgrading()) {
    echo "Moodle upgrade pending, backup execution suspended.\n";
    exit(1);
}

// now get cli options
list($options, $unrecognized) = cli_get_params(array('help'=>false,
        'timestamp'=>false, 'slowly'=>false),
        array('h'=>'help',
              't'=>'timestamp',
              's'=>'slowly'));

if ($unrecognized) {
    $unrecognized = implode("\n  ", $unrecognized);
    cli_error(get_string('cliunknowoption', 'admin', $unrecognized));
}

if ($options['help'] || !$options['timestamp']) {
    $help =
    "Purge moodle log and grade history tables up to a given timestamp.

Options:
-h, --help            Print out this help
-t, --timestamp       Purge logs before this unix timestamp (mandatory)
-s, --slowly	      Purge slowly so as to minimize site disruption

";

    echo $help;
    die;
}

$timestamp = $options['timestamp'];
$slowly = $options['slowly'];

$starttime = microtime();

/// emulate normal session
cron_setup_user();

/// Start output log
$timenow = time();

mtrace("Server Time: ".date('r',$timenow)."\n");

// Purge the logs
uwmoodle_util_helper::purge_logs($timestamp, $slowly);

mtrace("Completed");

$difftime = microtime_diff($starttime, microtime());
mtrace("Execution took ".$difftime." seconds");