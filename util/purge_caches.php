<?php
/**
 * This page purges the local moodle caches.
 *
 * This is intended only to be called from the uwmoodle_util_helper::purge_all_backend_caches()
 * function.  Authentication is via the uwmoodle hostkey parameter.
 *
 * usage:  hostkey=$hostkey
 *
 * @author    Matt Petro <petro@engr.wisc.edu>
 */

// don't cache this page
//
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");                                    // HTTP/1.0
header('Content-Type: text/plain; charset=utf-8');

// Run even during upgrade
define('NO_UPGRADE_CHECK', true);

// Disable html output.
define('AJAX_SCRIPT', true);

require_once('../../../config.php');
require_once('uwmoodle_util_helper.php');

uwmoodle_util_helper::verify_hostkey();

uwmoodle_util_helper::purge_local_caches();

echo "DONE.";
