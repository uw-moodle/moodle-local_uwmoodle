<?php
/**
 * This page stats the php files corresponding to the top MAX_STAT (M) entries in the apc opcode cache,
 * sorting by hit count. This is useful when moodle is on NFS so that infrequent page loads don't bog down
 * on NFS traffic statting all the files.  Note that disabling statting in apc is only a partial fix to
 * this problem.
 *
 * usage:  refresh.php?M=500
 *
 * @author    Matt Petro <petro@engr.wisc.edu>
 */

// don't cache this page
//
header("Cache-Control: no-store, no-cache, must-revalidate");  // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");                                    // HTTP/1.0
header('Content-Type: text/plain; charset=utf-8');

if(!function_exists('apc_cache_info') || !($cache=@apc_cache_info('opcode'))) {
    echo "No cache info available.  APC does not appear to be running.";
    exit;
}

$MAX_STAT = 600; // maximum number of files to stat

if (!empty($_GET['M'])) {
    $MAX_STAT = intval($_GET['M']);
}

// get apc opcode entries
$cache=@apc_cache_info('opcode');

// sort entries by num_hits, decreasing
usort($cache['cache_list'], function($a, $b) { if ($a['num_hits'] == $b['num_hits']) return 0; return ($a['num_hits'] < $b['num_hits'])? 1 : -1; });

$cnt = 0;
foreach ($cache['cache_list'] as $entry) {
    if ($cnt >= $MAX_STAT) {
        break;
    }
    $filename = $entry['filename'];
    stat($filename);
    $cnt++;
}

echo "$cnt of ".count($cache['cache_list'])." entries freshened.  \n";
echo "Done";