<?php

/**
 * Utility helper class.
 */


// Support both old and new versions of the wiscservices plugin
if (file_exists($CFG->dirroot.'/local/wiscservices/locallib.php')) {
    require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
} else if (file_exists($CFG->dirroot.'/local/wiscservices/lib.php')) {
    require_once($CFG->dirroot.'/local/wiscservices/lib.php');
}

require_once($CFG->dirroot.'/local/wiscservices/lib/peoplepicker.php');
require_once($CFG->dirroot.'/course/lib.php');

abstract class uwmoodle_util_helper {

    public static function fix_coursecontexts() {
        global $DB;

        $courses = $DB->get_records('course', array(), '', 'id, category, shortname');
        foreach ($courses as $course) {
            if ($course->category == 0) {
                continue;
            }
            mtrace("Fixing $course->shortname ($course->id)");
            $newparent = get_context_instance(CONTEXT_COURSECAT, $course->category);
            $context   = get_context_instance(CONTEXT_COURSE, $course->id);
            context_moved($context, $newparent);
        }
        fix_course_sortorder();
    }

    public static function fix_roles($userid) {
        global $DB;

        $user =  $DB->get_record('user', array('id'=>$userid), '*', MUST_EXIST);
        $courses = $DB->get_records('course', array(), '', 'id, category, shortname');
        foreach ($courses as $course) {
            if ($course->category == 0) {
                continue;
            }
            $context = get_context_instance(CONTEXT_COURSE, $course->id, MUST_EXIST);

            // Remove manual teacher role if the creator has access at the category level
            if (is_viewing($context, $user->id, 'moodle/role:assign')) {
                mtrace("Unenrolling $course->shortname ($course->id)");
                self::helper_unenrol_internal_user($course->id, $user->id);
            }
        }
    }

    public static function move_courses(array $courseids, $categoryid) {
        move_courses($courseids, $categoryid);
    }

    public static function helper_unenrol_internal_user($courseid, $userid) {
        global $DB;
        if (!$enrol = enrol_get_plugin('manual')) {
            return false;
        }
        if (!$instances = $DB->get_records('enrol', array('enrol'=>'manual', 'courseid'=>$courseid, 'status'=>ENROL_INSTANCE_ENABLED), 'sortorder,id ASC')) {
            return false;
        }
        $instance = reset($instances);

        $enrol->unenrol_user($instance, $userid);

    }

    public static function update_coursemaps() {
        global $OUTPUT, $DB, $CFG;
        require_once($CFG->dirroot.'/enrol/wisc/lib/datastore.php');
        // attempt to populate new coursemap fields
        echo $OUTPUT->notification("Updating coursemap entries. This might take a while...");
        try {
            $datastore = new \enrol_wisc\local\chub\chub_datasource();
            $terms = $datastore->getAvailableTerms();
            $termcodes = array();
            foreach ($terms as $term) {
                $termcodes[] = $term->termCode;
            }
            if (!empty($termcodes)) {
                $maps = $DB->get_recordset_list('enrol_wisc_coursemap', 'term', $termcodes);
                foreach ($maps as $map) {
                    $needsupdate = is_null($map->subject) || is_null($map->isis_course_id) || is_null($map->type);
                    if (!$needsupdate) {
                        continue;
                    }
                    try {
                        $class = $datastore->getClassByClassNumber($map->term, $map->class_number);
                        $update = new stdClass();
                        $update->id = $map->id;
                        $update->subject = $class->subject->shortDescription;
                        $update->isis_course_id = $class->courseId;
                        $update->type = $class->type;
                        $DB->update_record('enrol_wisc_coursemap', $update);
                        echo $OUTPUT->notification("Updating $map->term,  $map->class_number");
                    } catch (Exception $e) {
                        // ignore exceptions for individual courses
                        echo $OUTPUT->notification("Error fetching course $map->term,  $map->class_number : ".$e->getMessage());
                    }
                }
            }

        } catch(Exception $e) {
            // Likely the datastore isn't setup right
            echo $OUTPUT->notification("Failed: ". $e->getMessage());
            $result = false;
        }
    }

    public static function add_block($category, $new_block, $region, $weight) {
        global $OUTPUT, $DB, $CFG;

        throw new moodle_exception("Don't run this code.. it's broken.");

        require_once($CFG->libdir.'/blocklib.php');
        $courses = get_courses($category);//can be feed categoryid to just effect one category
        foreach($courses as $course) {
            if ($course->id !== SITEID) {
                $page = new moodle_page();
                $page->set_course($course);
                $page->blocks->add_regions(array($region));
                if (!$page->blocks->is_block_present($new_block)) {
                    echo "Adding to $course->shortname\n";
                    $page->blocks->add_block($new_block, $region, $weight, false, 'course-view-*');
                } else {
                    echo "Skipping $course->shortname\n";
                }
            }
        }
    }

    public static function enrol_users($courseshortname, $roleshortname, array $usernames) {
        global $OUTPUT, $DB, $CFG;


        $course = $DB->get_record('course', array('shortname'=>$courseshortname), '*', MUST_EXIST);
        if ($course->id == 0) {
            throw new moodle_exception("Invalid course.");
        }

        $wiscservices = new local_wiscservices_plugin();
        $peoplepicker = new wisc_peoplepicker();

        if (empty($roleshortname)) {
            throw new moodle_exception("Invalid role shortname.");
        }
        $studentrole = $DB->get_record('role', array('shortname'=>$roleshortname), '*', MUST_EXIST);

        $status = '===================\n';
        foreach ($usernames as $username) {
            $params = array('username'=>$username,
                            'deleted'=>0);
            $user = $DB->get_record('user', $params);

            $userid = null;
            if ($user) {
                $userid = $user->id;
            } else {
                $matches = array();
                if (preg_match('/(.*)@wisc.edu$/i', $username, $matches)) {
                    $netid = $matches[1];
                    $person = $peoplepicker->getPeopleByNetid(array($netid));
                    $person = reset($person);
                    if ($person) {
                        $userid = $wiscservices->verify_person($person);
                    }
                }
            }
            if (!$userid) {
                mtrace("Error creating: $username");
                continue;
            }
            $res = enrol_try_internal_enrol($course->id, $userid, $studentrole->id);
            if (!$res) {
                mtrace("Error enrolling: $username");
                continue;
            }
            $status.="Enrolled $username as $studentrole->shortname\n";
        }

        echo $status;
    }

    public static function purge_logs($timestamp, $slowly) {
        global $DB;

        self::purge_log_table('grade_outcomes_history', 'timemodified', $timestamp, $slowly);
        self::purge_log_table('grade_categories_history', 'timemodified', $timestamp, $slowly);
        self::purge_log_table('grade_items_history', 'timemodified', $timestamp, $slowly);
        self::purge_log_table('grade_grades_history', 'timemodified', $timestamp, $slowly);
        self::purge_log_table('scale_history', 'timemodified', $timestamp, $slowly);
        self::purge_log_table('log', 'time', $timestamp, $slowly);
    }

    // Number of records to delete at one time, when deleting slowly
    const PURGE_BULK_RECORDS = 1000;

    // If query takes more than this many seconds, step down the query size
    const PURGE_MAX_TIME = 5;

    // If query takes less than this many seconds, step up the query size
    const PURGE_MIN_TIME = 1;

    // Number of seconds to sleep in between deletes
    const PURGE_SLEEP = 5;

    public static function purge_log_table($table, $timecolumn, $timestamp, $slowly) {
        global $DB;

        mtrace("Processing $table");
        $sqlselect = "$timecolumn < ?";
        $params = array($timestamp);
        if (!$slowly) {
            $DB->delete_records_select($table, $sqlselect, $params);
        } else {
            $bulk = self::PURGE_BULK_RECORDS;
            while ($DB->record_exists_select($table, $sqlselect, $params)) {
                $limitsql = " LIMIT $bulk";
                $starttime = microtime();
                $DB->delete_records_select($table, $sqlselect . $limitsql, $params);
                $difftime = microtime_diff($starttime, microtime());
                mtrace("deleted $bulk records ($difftime s)");

                // step up/down limit as appropriate
                if ($difftime > self::PURGE_MAX_TIME) {
                    $bulk  = max(1, intval($bulk * self::PURGE_MAX_TIME / $difftime));
                } else if ($difftime < self::PURGE_MIN_TIME) {
                    $bulk  = $bulk * 2;
                }
                sleep(self::PURGE_SLEEP);
            }
        }
        mtrace('Done');
    }

    public static function create_anon_users($count) {
        global $DB, $CFG;

        // TODO: We should create user contexts, and maybe call event handlers, etc.  Otherwise we rely on cron to fix up the accounts.

        for ($i = 1; $i <= $count; ++$i) {
            $username = "anon$i";
            if (!$DB->record_exists('user', array('username'=>$username))) {
                //create anon user
                $user = new stdClass;
                $user->username = $username;

                $user->firstname = "anonfirstname$i";
                $user->lastname = "anonlastname$i";
                $user->city = 'Perth';
                $user->country = 'AU';
                $user->lastip = '127.0.0.1';
                $user->password = 'restored';
                $user->email = "anon$i@doesntexist.com";
                $user->emailstop = 1;
                $user->confirmed  = 1;
                $user->timecreated   = time();
                $user->timemodified  = 0;
                $user->mnethostid = $CFG->mnet_localhost_id;
                $user->lang = $CFG->lang;

                $user->id = $DB->insert_record('user', $user);
                echo '+';
            } else {
                echo '.';
            }
        }
    }

    /**
     * Generate a new host key.
     *
     * The host key allows us to authenticate to our own moodle instance
     * using a shared key.  This is used in accessing other moodle backends to
     * purge caches.
     *
     */
    public static function init_hostkey() {
        $hostkey = random_string(32);
        set_config('hostkey', $hostkey, 'local_uwmoodle');
        return $hostkey;
    }

    /**
     * Return host key.
     *
     * @return string
     */
    public static function get_hostkey() {
        $hostkey = get_config('local_uwmoodle', 'hostkey');
        if ($hostkey === false) {
            $hostkey = self::init_hostkey();
        }
        return $hostkey;
    }

    public static function verify_hostkey() {
        $hostkey = optional_param('hostkey', '', PARAM_TEXT);
        if (empty($hostkey) || $hostkey !== self::get_hostkey()) {
            throw new moodle_exception('hostkeyverifyfailed', 'local_uwmoodle');
        }
        return true;
    }

    /**
     * Purge the local cache directories.  This is the part of
     * purge_all_caches which needs to be done on each backend independently.
     */
    public static function purge_local_caches() {
        global $CFG;

        if (empty($CFG->clusterlocalcachename)) {
            // No local cluster cache.
            return;
        }

        // The following does nothing if the cache isn't installed.
        cache_helper::purge_store($CFG->clusterlocalcachename);

    }

    /**
     * Return an array containing all backends for this host.
     *
     * @param bool $includethishost  include ourselves as well
     * @return array of the form  hostname => IPv6 address
     */
    public static function get_all_backends($includethishost) {
        global $CFG;
        $vhost = parse_url($CFG->wwwroot, PHP_URL_HOST);
        $results = dns_get_record("$vhost.vhcluster.cae.wisc.edu", DNS_TXT);
        $backends = array();
        foreach ($results as $rr) {
            $nodename = $rr['entries'][0];
            $aaaa = $rr['entries'][1];
            if (!$includethishost && isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == $aaaa) {
                continue;
            }
            if (!empty($nodename) && preg_match('/^2607:f388:1082:ff:[a-f0-9:]+$/', $aaaa) == 1) {
                $backends[$nodename] = $aaaa;
            }
        }
        return $backends;

    }

    /**
     * Load a page on all backends
     *
     * @param string $url
     * @param bool $includethishost  whether to include ourselves as well
     * @return string status message
     */
    public static function load_page_on_all_backends($url, $includethishost) {

        $relurl = parse_url($url, PHP_URL_PATH).'?'.parse_url($url,  PHP_URL_QUERY);
        $host = parse_url($url, PHP_URL_HOST);
        $hostkey = self::get_hostkey();
        $backends = self::get_all_backends($includethishost);
        $output = "";

        if (empty($backends)) {
            return "No additional backends found from DNS information";
        }

        $output = "Found additional backends: ".implode(', ', array_keys($backends))."\n\n";

        // create the curl handles
        $chandles = array();
        foreach ($backends as $backend => $aaaa) {
            $localurl = "https://$aaaa:443".$relurl;
            $ch = curl_init($localurl);
            curl_setopt($ch, CURLOPT_VERBOSE, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, array('hostkey'=>$hostkey));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: $host"));

            // ignore cert, by necessity as we are connecting via IP address
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

            $chandles[$backend] = $ch;
        }

        //create the multiple curl handle
        $mh = curl_multi_init();
        foreach ($chandles as &$ch) {
            $res = curl_multi_add_handle($mh, $ch);
        }

        //execute the handles
        $active = null;
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($mh) != -1) {
                do {
                    $mrc = curl_multi_exec($mh, $active);

                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }
        }

        // get the results
        $results = array();

        // read curl_multi status info
        while ($msg = curl_multi_info_read($mh)) {
            $backend = array_search($msg['handle'], $chandles, true);
            if (empty($backend)) {
                // shouldn't happen
                error_log('Unexpected return from curl_multi_info_read()\n');
                continue;
            }
            if ($msg['result'] == CURLE_OK) {
                $results[$backend] = "SUCCESS";
            } else {
                $results[$backend] = "CURL ERROR ".$msg['result'];
            }
        }

        // read the output from each handle
        foreach ($chandles as $backend => &$ch) {
            $output .= "[$backend]: ".$results[$backend];
            if (curl_errno($ch))
            {
                $output .= " - ".curl_error($ch);
            }
            $content = curl_multi_getcontent($ch);
            $content = substr($content, 0, 132); // truncate output
            $output .= " (".$content.")\n";
        }

        //close the handles
        foreach ($chandles as &$ch) {
            curl_multi_remove_handle($mh, $ch);
            curl_close($ch);
        }
        curl_multi_close($mh);

        return $output;
    }

    /**
     * Purge moodle caches across all backends, excluding the current backend.  The assumption is that the
     * current backend cache has already been flushed when this is called.
     *
     * @return string curl output for all backends
     */
    public static function purge_all_backend_caches() {
        set_config('lastcachepurge', time(), 'local_uwmoodle');
        return self::load_page_on_all_backends(new moodle_url('/local/uwmoodle/util/purge_caches.php'), false);
    }

}
